# java-swing-demo

Découverte de la librairie Swing en Java

## Un peu de théorie

Java implémente 3 librairies graphiques : 

  * AWT
  * Swing
  * JavaFX

Chacune de ces librairies a ses avantages et inconvénients.  
AWT et Spring sont les librairies "legacy" et ne bénéficient plus que d'une maintenance de sécurité actuellement.

JavaFX est "la" manière recommandée par Oracle pour créer des interfaces graphiques modernes en Java, mais elle ne bénéficie pas de la même popularité que les deux précédentes.

## Ressources

  * Les bases de Swing et les composants disponibles : https://www.javatpoint.com/java-swing
  * La gestion des événements : https://www.javatpoint.com/event-handling-in-java ou https://www.jmdoudoux.fr/java/dej/chap-event.htm 
  * Les layouts : https://docs.oracle.com/javase/tutorial/uiswing/layout/using.html
  * Configurer VS Code pour ajouter l'auto-complétion AWT : https://code.visualstudio.com/docs/java/java-gui#_develop-awt-applications


## Exercice 1

  * Créer une classe Pizza qui a un attribut "prix", que vous fixerez à 9,50€
  * Créer une fenêtre avec le titre "Ma pizzeria"
  * Configurer la fenêtre pour quitter l'application quand on clique sur la croix de fermeture
  * Ajouter un label centré en haut, avec pour valeur "Caisse automatique"
  * Ajouter un label "Nombre de pizzas" et un composant texte pour faire saisir à l'utilisateur un nombre de pizzas
  * Ajouter un bouton "Calculer le total"
  * Ajouter un label permettant d'afficher le résultat du calcul

## Exercice 2

  * Mettre en place la gestion du clic sur le bouton "Calculer le total" : quand l'utilisateur clique, on affiche dans le label de résultat le prix à payer (quantité saisie x prix d'une pizza)
  * **ATTENTION** : votre application ne doit JAMAIS crasher. Comment faites-vous ? Proposez au moins 2 solutions !

## Exercice 3

  * Ajouter une case à cocher "Supplément fromage", qui rajoute 1,50€ au total. Quand l'utilisateur clique sur le bouton "Calculer le total", l'application doit calculer le prix des pizzas, et rajouter éventuellement le supplément
  * Ajouter un spinner pour des sauces piquantes, à 0,20€ pièce. À chaque fois que l'utilisateur rajoute une sauce, le total se met à jour automatiquement

## Exercice 4

  * Ajouter des boutons radios "Sur place" et "À emporter".
    * Aucune valeur par défaut
    * Si l'utilisateur clique sur le bouton "Calculer le total" sans avoir sélectionné de valeur, il faut le prévenir et ne pas calculer
    * Si l'utilisateur a choisi "À emporter", il faut rajouter 10€ de livraison


## Exercice 5

  * Créez une classe Margarita, qui hérite de la classe Pizza, mais coûte 9€
  * Créez une classe Cannibale, qui hérite de la classe Pizza, mais qui coûte 11€
  * Créez une classe Calzone, qui hérite de la classe Pizza, et qui coûte 10,50€
  * L'utilisateur ne doit plus pouvoir créer de pizzas, seulement des Margarita, des Cannibale, et des Calzone, comment faites-vous ?
  * Créez une interface graphique pour faire choisir à l'utilisateur quel type de pizza il souhaite acheter, et en fonction du type choisi, afficher dans un label le prix de la pizza

## Exercice 6

  * Créer une interface graphique avec 2 onglets "Nouvelle commande" et "Liste des commandes"
  * Dans le premier onglet, afficher une liste déroulante des pizzas, un élément pour faire saisir la quantité désirée, et un bouton "Passer la commande"
  * Sur le deuxième onglet, afficher la liste de toutes les commandes passées, avec une colonne pour le type de pizza, une commande pour la quantité, et une commande pour le prix payé

## Exercice 7
  * Insérez une étape de validation avant l'ajout à la commande : lorsque l'utilisateur clique sur "Passer la commande", afficher le prix total à payer, et faire valider ou annuler la commande à l'utilisateur.
    * si la commande est validée, alors l'ajouter à la liste puis réinitialiser les champs
    * si la commande est annulée, réinitialiser les champs mais ne pas enregistrer la commande
